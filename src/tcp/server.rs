
use std::sync::mpsc;
use super::client::SimpleTcpClient;
use super::super::Server;

use std::net::TcpListener;
use std::thread;


pub struct SimpleTcpServer {
	listener: TcpListener,
}

impl<'a> From<&'a str> for SimpleTcpServer {
	fn from(addr: &'a str) -> SimpleTcpServer {
		// let addr = addr.parse().unwrap();
		let listener = TcpListener::bind(addr).unwrap();
		SimpleTcpServer::from(listener)
	}
}


impl From<TcpListener> for SimpleTcpServer {
	fn from(listener: TcpListener) -> SimpleTcpServer {
		SimpleTcpServer {
			listener,
		}
	}
}


impl Server for SimpleTcpServer {
	type Client = SimpleTcpClient;
	fn start(self) -> mpsc::Receiver<Self::Client> {
		let (sender, receiver) = mpsc::channel();
		let listener = self.listener;
		thread::spawn(move || {
			while let Some(stream) = listener.incoming().next() {
				match stream {
					Ok(stream) => {
						let client = SimpleTcpClient::from(stream);
						sender.send(client).unwrap();
					},
					Err(err) => panic!("{:?}", err),
				}
			}
		});
		receiver
	}
}


