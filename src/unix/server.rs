
use std::sync::mpsc;
use super::client::SimpleUnixClient;
use super::super::Server;

use std::os::unix::net::UnixListener;
use std::fs;
use std::thread;


pub struct SimpleUnixServer {
	listener: UnixListener,
}

impl<'a> From<&'a str> for SimpleUnixServer {
	fn from(addr: &'a str) -> SimpleUnixServer {
		// let addr = addr.parse().unwrap();
		let listener = match UnixListener::bind(addr) {
			Ok(sock) => sock,
			Err(_) => {
				fs::remove_file(addr).unwrap();
				UnixListener::bind(addr).unwrap()
			}
		};
		SimpleUnixServer::from(listener)
	}
}


impl From<UnixListener> for SimpleUnixServer {
	fn from(listener: UnixListener) -> SimpleUnixServer {
		SimpleUnixServer {
			listener,
		}
	}
}


impl Server for SimpleUnixServer {
	type Client = SimpleUnixClient;
	fn start(self) -> mpsc::Receiver<Self::Client> {
		let (sender, receiver) = mpsc::channel();
		let listener = self.listener;
		thread::spawn(move || {
			while let Some(stream) = listener.incoming().next() {
				match stream {
					Ok(stream) => {
						let client = SimpleUnixClient::from(stream);
						sender.send(client).unwrap();
					},
					Err(err) => panic!("{:?}", err),
				}
			}
		});
		receiver
	}
}


