
use std::sync::{Arc, Mutex, mpsc};

use super::session::Sessions;
use super::super::Message;
use super::super::Response;
use super::super::Data;
use super::super::Code;

#[derive(Clone)]
pub struct Sender {
	sender: mpsc::Sender<(usize, Message)>,
	sessions: Arc<Mutex<Sessions>>,
}

impl From<(mpsc::Sender<(usize, Message)>, Arc<Mutex<Sessions>>)> for Sender {
	fn from(args: (mpsc::Sender<(usize, Message)>, Arc<Mutex<Sessions>>)) -> Sender {
		Sender {
			sender: args.0,
			sessions: args.1,
		}
	}
}

impl Sender {
	pub fn send(&mut self, cid: usize, code: Code, data: Data) -> Response {
		let (mid, receiver) = {
			let mut sessions = self.sessions.lock().unwrap();
			sessions.reg(cid)
		};
		let msg = (code, mid, data);
		self.sender.send((cid, msg)).unwrap();
		receiver.recv().unwrap()
	}
}
