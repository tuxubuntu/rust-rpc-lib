
mod session;
pub mod sender;

use std::thread;
use self::sender::Sender;
use self::session::Sessions;
use std::collections::HashMap;
use std::sync::{Arc, Mutex, mpsc};
use super::Router;
use super::Client;
use super::Rpc;
use super::Server;
use super::Message;
use std::marker::PhantomData;

pub struct SimpleRpc<R: Router, C: Client> {
	sessions: Arc<Mutex<Sessions>>,
	router: Arc<Mutex<R>>,
	clients: Arc<Mutex<HashMap<usize, C>>>,
	counter: Arc<Mutex<usize>>,
	clients_channel: Arc<Mutex<Option<mpsc::Sender<usize>>>>,
	writer_channel: mpsc::Sender<(usize, Message)>,
	_c: PhantomData<C>
}

impl<R, C: 'static> From<R> for SimpleRpc<R, C> where R: Router, C: Client {
	fn from(router: R) -> SimpleRpc<R, C> {
		let sessions = Arc::new(Mutex::new(Sessions::new()));
		let clients = Arc::new(Mutex::new(HashMap::new()));
		let counter = Arc::new(Mutex::new(0));
		let router = Arc::new(Mutex::new(router));
		let clients_channel = Arc::new(Mutex::new(None));
		let (sender, receiver) = mpsc::channel();
		let clients_in = clients.clone();
		let sessions_in = sessions.clone();
		thread::spawn(move || {
			start_writer(receiver, clients_in, sessions_in);
		});
		SimpleRpc {
			sessions,
			router,
			clients,
			counter,
			clients_channel,
			writer_channel: sender,
			_c: PhantomData,
		}
	}
}


impl<R: 'static, C: 'static> Rpc<C> for SimpleRpc<R, C>
where
	C: Client,
	R: Router,
{
	fn listen<S: 'static>(&mut self, server: S)
	where
		S: Server<Client=C>
	{
		let router = self.router.clone();
		let clients_channel = self.clients_channel.clone();
		let counter = self.counter.clone();
		for client in server.start() {
			let counter = counter.clone();
			let cid = gen_cid(counter);
			let router = router.clone();
			let mut clients_channel = clients_channel.lock().unwrap();
			if let Some(clients_channel) = clients_channel.take() {
				clients_channel.send(cid).unwrap();
			}
			let sessions = self.sessions.clone();
			let writer = self.writer_channel.clone();
			let clients = self.clients.clone();
			thread::spawn(move || {
				SimpleRpc::launch_client(client, cid, router, sessions, clients, writer);
			});
		}
	}
	fn connect(&mut self, client: C) {
		let router = self.router.clone();
		let cid = gen_cid(self.counter.clone());
		let mut clients_channel = self.clients_channel.lock().unwrap();
		if let Some(clients_channel) = clients_channel.take() {
			clients_channel.send(cid).unwrap();
		}
		let sessions = self.sessions.clone();
		let writer = self.writer_channel.clone();
		let clients = self.clients.clone();
		SimpleRpc::launch_client(client, cid, router, sessions, clients, writer);
	}
	fn on_connect(&mut self) -> mpsc::Receiver<usize> {
		let (sender, receiver) = mpsc::channel();
		let clients_channel = self.clients_channel.clone();
		let mut clients_channel = clients_channel.lock().unwrap();
		*clients_channel = Some(sender);
		receiver
	}
	fn sender(&self) -> Sender {
		let writer = self.writer_channel.clone();
		let sessions = self.sessions.clone();
		Sender::from((writer, sessions))
	}
}

fn gen_cid(counter: Arc<Mutex<usize>>) -> usize {
	let mut counter = counter.lock().unwrap();
	let cid = *counter;
	*counter = cid+1;
	cid
}

fn start_writer<C: Client>(
	writer: mpsc::Receiver<(usize, Message)>,
	clients: Arc<Mutex<HashMap<usize, C>>>,
	sessions: Arc<Mutex<Sessions>>,
) {
	for (cid, msg) in writer.iter() {
		let mut clients = clients.lock().unwrap();
		if let Some(client) = clients.get_mut(&cid) {
			client.write(msg).unwrap();
		} else {
			sessions.lock().unwrap().resolve(cid, msg.1, Err(String::from("Client not found.")));
		}
	}
}

impl<R: 'static, C: 'static> SimpleRpc<R, C>
where
	R: Router,
	C: Client
{
	fn launch_client (
		mut client: C,
		cid: usize,
		router: Arc<Mutex<R>>,
		sessions: Arc<Mutex<Sessions>>,
		clients: Arc<Mutex<HashMap<usize, C>>>,
		writer: mpsc::Sender<(usize, Message)>
	) {
		{
			let client_w = client.clone();
			clients.lock().unwrap().insert(cid, client_w);
		}
		loop {
			match client.read() {
				Ok(msg) => {
					let (code, mid, data) = msg;
					let has = {
						sessions.lock().unwrap().has(cid, mid)
					};
					if has {
						let res = if code == 0 {
							Ok(data)
						} else {
							use std::str;
							let s = str::from_utf8(&data).unwrap();
							let s = String::from(s);
							Err(s)
						};
						sessions.lock().unwrap().resolve(cid, mid, res);
					} else {
						let mut router = router.lock().unwrap();
						let msg = if let Some(handler) = router.handler(code) {
							let sender = Sender::from((writer.clone(), sessions.clone()));
							match handler(cid, data, sender) {
								Ok(data) => (0, mid, data),
								Err(err) => {
									let data = err.into_bytes().to_vec();
									(1, mid, data)
								},
							}
						} else {
							let data = String::from("Endpoint not found.");
							let data = data.into_bytes().to_vec();
							(1, mid, data)
						};
						writer.send((cid, msg)).unwrap();
					}
				},
				Err(err) => {
					println!("Connection error: {:?}", err);
					break;
				},
			}
		};
	}
}

