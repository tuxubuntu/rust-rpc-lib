use super::Message;
use super::Data;
use super::Result;

use containerizer::{
	Container,
	raw::{ RawLen32, RawLen64 }
};

use message;

pub fn encode(msg: Message) -> Result<Data> {
	match message::Message::encode((msg.0, msg.1, msg.2)) {
		Ok(data) => {
			use std;
			if data.len() < std::u32::MAX as usize {
				match RawLen32::encode(data) {
					Ok(data) => Ok(data),
					Err(err) => Err(String::from(err)),
				}
			} else {
				match RawLen64::encode(data) {
					Ok(data) => Ok(data),
					Err(err) => Err(String::from(err)),
				}
			}
		},
		Err(err) => Err(String::from(err)),
	}
}

pub fn decode(body: Data) -> Message {
	message::Message::decode(body).unwrap()
}
